<!DOCTYPE html>  <!-- récupère l'id et le type du matériel choisi et demande le nom de l'emprunteur -->
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/index.css"
  </head>
  <body>
    <p>Remplissez ce formulaire si vous voulez emprunter ce matériel</p>
    </br>
     <form method='post' action='borrow.php'> <!-- renvoie les info les borrow.php -->
       <p>Numéro du matériel</p>
       <input type="text" name="id" value=<?php echo $_GET['id']; ?>><br><br>    <!-- récupère l'id du matériel choisi -->
       <p>Matériel souhaité</p>
       <input type="text" name="m" value=<?php echo $_GET['m']; ?>><br><br>  <!-- récupère le type du matériel choisi -->
       <p>Votre nom et prénom</p>
       <input type="text" name="usr"><br><br>    <!-- récupère le nom de l'emprunteur -->
       <input type="submit" value="Emprunter">
     </form>
     </br> </br>

      <p>Remplissez ce formulaire si vous voulez rendre ce matériel</p>
      </br>
       <form method='post' action='rendre.php'>  <!-- renvoie vers le fichier rendre.php -->
         <p>Numéro du matériel</p>
         <input type="text" name="id" value=<?php echo $_GET['id']; ?>><br><br>
         <p>Matériel souhaité</p>
         <input type="text" name="m" value=<?php echo $_GET['m']; ?>><br><br>
         <input type="submit" value="Rendre">
       </form>
  </body>
</html>
