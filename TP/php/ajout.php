<!DOCTYPE html>  <!-- traite les infos récupérées pour mettre à jour la BDD -->
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>
  <body>
    <?php
      echo "Vous avez ajouté:";
      echo "</br>";
      echo  $_POST['Idvalue'];
      echo "</br>";
      echo $_POST['Mvalue'];
      echo "</br>";

      // Se connecter à la base de donneer

      $bdd= new PDO("mysql:host=localhost;dbname=gestion_materiel;", "root", "");


      $sql="INSERT INTO materiel (id_Mat, type_Mat, statut) VALUES (:Idvalue, :Mvalue, 'dispo')";  //requête pour ajouter les données saisie

      $req = $bdd->prepare($sql);
      $req->execute (array(
        'Idvalue' => $_POST['Idvalue'],
        'Mvalue' => $_POST['Mvalue']
      ));

     ?>

     <input type="button" value="retour à la liste du materiel" onclick="javascript:location.href='index.html'">  <!-- rediriger vers page la page d'acceuille -->

  </body>
</html>
