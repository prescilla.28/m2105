<!DOCTYPE html>  <!-- traite les infos récupérées pour mettre à jour la BDD -->
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/index.css">
  </head>
  <body>
    <?php
      // Faire arriver ici l'ID du matériel et le nom emprunteur
      echo "Vous avez emprunté:";
      echo "</br>";
      echo $_POST['id'];
      echo "</br>";
      echo $_POST['m'];
      echo "</br>";
      echo $_POST['usr'];
      echo "</br>";



      $bdd= new PDO("mysql:host=localhost;dbname=gestion_materiel;", "root", "");  // Se connecter à la base de donnée

      // la requete c'est un update
      $sql="SELECT statut FROM materiel WHERE id_Mat=:gid";
      $sql= "UPDATE materiel SET statut = 'non_dispo', Nom_Ut =:usr WHERE id_Mat=:gid" ; // - update le champ avec nom utilisateur - where id_Mat = $_POST['id']

      $req = $bdd->prepare($sql);
      $req->execute (array(
        'gid' => $_POST['id'],
        'usr' => $_POST['usr']
      ));

     ?>
     <!-- rediriger vers page check2.php -->
     <input type="button" value="retour à la liste du materiel" onclick="javascript:location.href='index.html'">

  </body>
</html>
