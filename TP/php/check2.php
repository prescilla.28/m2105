<!DOCTYPE html>  <!-- traitement pour connection à la BDD -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
  </head>
  <body>

    <table class="bdd">
    <?php
        if($_POST["Nom"] =="root" && $_POST["mdp"]=="root"){

            try{
            $bdd= new PDO("mysql:host=localhost;dbname=gestion_materiel;", "root", "");  //connection à la base de données en tant que Admin réussie
            echo"Connexion réussie";
            echo "<br/>";
            echo "<br/>";
            echo"Cliquez sur le matériel souhaité si vous voulez le supprimer ou modifier";
            echo"<br/>";
            }
            catch(Exception $ex){   //connection non réussie
            die('Erreur :'.$ex->getMessage());
            echo"Erreur de connexion";
            }
            $sql="SELECT id_Mat, type_Mat, statut, Nom_Ut FROM materiel";   //affichage de la table materiel sous forme de tableau

            $reponse=$bdd->query($sql);
            echo "
                <tr>
                    <td><b>id_Mat</b></td>
                    <td><b>type_Mat</b></td>
                    <td><b>statut</b></td>
                    <td><b>Nom_Ut</b></td>
                </tr>";

            foreach ($reponse as $row){ //'bouton' type_Mat redirige vers Admin.php en prennant id_Mat et type_Mat
            echo"<tr>
            <td>" .$row['id_Mat']. "</td>
            <td><a href=Admin.php?id=" .$row['id_Mat']. "&m=".$row['type_Mat'].">".$row['type_Mat']."</a></td>
            <td>" .$row['statut']."</td>
            <td>" .$row['Nom_Ut']."</td>
            </tr>";
          }
          ?>
          <input type="button" value="Ajouter un nouveau matériel" onclick="javascript:location.href='AdminAjout.php'">
          <?php

        }elseif ($_POST["Nom"] =="Prescilla" && $_POST["mdp"]=="37000405") {  //connection à la BDD en tant qu'élève réussie
          try{
          $bdd= new PDO("mysql:host=localhost;dbname=gestion_materiel;", "root", "");  //connection à la base de données réussie
          echo"Connexion réussie";
          echo "<br/>";
          echo "<br/>";
          echo"Veuillez choisir le matériel que vous voulez emprenter ou rendre";
          echo"<br/>";
          }
          catch(Exception $ex){   //connection non réussie
          die('Erreur :'.$ex->getMessage());
          echo"Erreur de connexion";
          }
          $sql="SELECT id_Mat, type_Mat, statut FROM materiel";   //affichage de la table materiel sous forme de tableau

          $reponse=$bdd->query($sql);
          echo "
              <tr>
                  <td><b>id_Mat</b></td>
                  <td><b>type_Mat</b></td>
                  <td><b>statut</b></td>

              </tr>";

          foreach ($reponse as $row){
          echo"<tr>
          <td>" .$row['id_Mat']. "</td>
          <td><a href=emprunt.php?id=" .$row['id_Mat']. "&m=".$row['type_Mat'].">".$row['type_Mat']."</a></td>
          <td>" .$row['statut']."</td>

          </tr>";
          }



          }
          else{
              header('Location: index.html?auth=err');  //message d'érreur dans le cas où erreur de connexion
              }
        ?>


      </table>
  </body>
</html>
