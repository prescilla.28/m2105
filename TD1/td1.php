<?php
    $pseudo = "Bob";
    echo "Bonjour $pseudo, la forme?";
    echo "<br/>";
    echo 'Bonjour $pseudo, la forme? ';
    $Nom = "Sponge";
    $Prenom = "Bob";
    echo "<br/>";
    echo "<br/>";

    echo "Exo 3: Tableau indexé";
    echo "<br/>";
    print_r($Nom);
    echo "<br/>";
    var_dump($Prenom); 
    #var_dump donne le type de la variable et combien de caractère il contient 
    echo "<br/>";
    $tableau = array('Salade', 'Tomate', 'Oignons');
    echo "<ul>";
        foreach($tableau as $valeur){
        echo "<li>$valeur </li><br/>";
        }
    echo"</ul>";

    echo "<br/>";
    echo "<br/>";
    echo"Exo 4: Tableau associatif";
    $menu = array(
        'Lundi'=>'Sauté mines',
        'Mardi'=>'Pain bouchons',
        'Mercredi'=>'Riz cantonais',
        'Jeudi'=>'Boucané bringelles',
        'Vendredi'=>'Cari zourites');
 
    echo"<ol>";
        foreach($menu as $cle => $valeur){
            echo "<li> $cle: $valeur </li>";
        }
    echo"</ol>";
    
    
?>