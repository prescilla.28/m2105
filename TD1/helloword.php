<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <title> Mon premier php </title>
    </head>
    <body>
        <!-- Methode1 --> 
        <h1>Hello world!</h1>
        <!-- Methode2 --> 
        <h1>
            <?php
            echo "Hello world !";
            ?>
        </h1>
        <!-- Methode3 --> 
        <?php
        echo "<h1>Hello world ! </h1>";
        ?>
        <!-- Methode4 --> 
        
        <?php
        $texte = "<h1>Hello world !</h1>";
        echo $texte;
        ?>
    </body>
         
</html>

<!-- quest 1 = il ne se passe rien/ erreur. en ouverture direct pas d'exécution du code php qui est dans le ficher -->
